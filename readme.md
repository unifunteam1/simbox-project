# Simbox project description
**General goals:** Create an application that will allow users to make phone calls and send sms messages, ussd requests via web. Only authorized users are allowed to access the services.
**Components**

 1. Front-end application (using **Angular**) with the following pages:
	* Login.
	* Simbox List. With MNO list as modal or within the same page.
	* Settings, confiugration.
 2. JAVA Back-end Appliccation.
 	* Rest API for FE App
 	* ARI Manager (ari4java, java asterisk)
 	* Database (MariaDB)
 	* Token-based Authentication
 3. Local Raspberry PI devices running Asterisk 13, with attached 3G Modems.

***
###### Project Setup (Docker)
Install Docker and setup your docker hub account.

###### Asterisk (docker container)
1. Setup Asterisk Image : docker pull costea/asterisk:firsttry  (repo Link : https://cloud.docker.com/u/costea/repository/docker/costea/asterisk)
2. Create container:
	```
		docker run --name aster -p 10000-10009 -p 10000-10009/udp -p 8088:8088 -p 8089:8089 -p 5060:5060 -p 5060:5060/udp -P -d YourImageId```
-  Edit: /etc/asterisk/rtp.conf port range to be between  *10000-10009*
- in /etc/asterisk/sip.conf find NAT SUPPORT section and add:

	```
	localnet=192.168.0.0/255.255.0.0
	externaddr = YourIp
	nat=yes```
- Configure sipjs in asterisk container from : https://sipjs.com/guides/server-configuration/asterisk/


###### Mariadb
1. Set up Mariadb: ```docker run --name mariadb -p 3306:3306 -e MYSQL_ROOT_PASSWORD=1234 -d mariadb```
2. Run Mariadb in Dbeaver application. New Database Connection -> MariaDb -> connect
3. Create "simbox" DataBase.

###### Ldap authentication
1. Setup local LDAP server: ```docker run --name ldap-service --hostname ldap-service --env LDAP_ORGANISATION="Fun International" --env LDAP_DOMAIN="fun.in" -p 389:389 -p 636:636 --detach osixia/openldap:1.1.8```
2. Set up LDAP admin to access the LDAP server DB: ```docker run -p 6443:443 -p 6080:80 --name phpldapadmin-service --hostname phpldapadmin-service --link ldap-service:ldap-host --env PHPLDAPADMIN_LDAP_HOSTS=ldap-host --detach osixia/phpldapadmin:0.7.2```
3. Access https://localhost:6443 using the following credentials: 	*Login DN: cn=admin,dc=fun,dc=in
	Password: admin*
4. Add users. [Here](https://www.techrepublic.com/article/how-to-populate-an-ldap-server-with-users-and-groups-via-phpldapadmin/) is a good guide on how to do that.


***
##### Java App description
Initalize a Maven project using [quarkus](https://quarkus.io/get-started/). Add all the required dependencies like ari4java, mariadb... in *pom.xml*
###### Endpoints
- Auth
- SimboxList
- ReqFilter

###### ARI Manager
Description



###### Database
1. Generate tables using model classes and annotations.
2. Use Entitymanager for all db operations.
***
##### Angular App description
Modules:
* Login
* SimboxList
* ...

Services:
* Authservice
* ...
