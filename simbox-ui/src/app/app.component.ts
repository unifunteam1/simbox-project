import { Component } from '@angular/core';
import { ToggleService } from './services/toggle.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'simbox-ui';

  constructor(private toggleService: ToggleService) {}
  getClass() {
    const classtoggle = {
      'pinned-sidebar': this.toggleService.getSidebarStat().isSidebarPinned
    }
    return classtoggle;
  }
}
