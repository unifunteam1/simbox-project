import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class AlertService {

  haserror: boolean = false;
  errormsg: string = "";

  constructor() { }

  reset() {
    this.errormsg = "";
    this.haserror = false;
  }

  setErrorMessage(error: string) {
    this.errormsg = error;
    this.haserror = true;
  }
}
