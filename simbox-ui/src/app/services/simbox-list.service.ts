import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Simbox } from '../interfaces/SimBox';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SimboxListService {

  constructor(private http: HttpClient) { }

  getSimboxes(): Observable<Simbox[]> {
    return this.http.get<Simbox[]>(`${environment.apiUrl}/simboxlist/getAll`);
  }
  deleteSimbox(simbox: Simbox): Observable<Simbox> {
    if (!simbox.deleted) 
      return this.http.post<Simbox>(`${environment.apiUrl}/simboxlist/delete`,simbox);
  }
  addSimbox(simbox: Simbox): Observable<Simbox> {
    return this.http.post<Simbox>(`${environment.apiUrl}/simboxlist/add`,simbox);
  }
  getSimboxbyId(id): Observable<Simbox> {
    return this.http.get<Simbox>(`${environment.apiUrl}/simboxlist/getById?id=`+id);
  }
  editSimbox(simbox: Simbox): Observable<Simbox> {
    return this.http.post<Simbox>(`${environment.apiUrl}/simboxlist/edit`,simbox);
  }
}
