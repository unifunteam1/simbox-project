import { TestBed } from '@angular/core/testing';

import { SimboxListService } from './simbox-list.service';

describe('SimboxListService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SimboxListService = TestBed.get(SimboxListService);
    expect(service).toBeTruthy();
  });
});
