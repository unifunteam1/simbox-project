import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';
import { Location } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService {

  constructor(private router: Router, private authService: AuthService, private location: Location) { }

  // canActivate() {
    // if(this.authService.isLoggedIn()){
    //   return true;
    // }
    // this.router.navigate(['/login']);
    // return false;
// }
canActivate() {
  if (!this.location.path().includes('/settings') && this.authService.checktokeStatus())
      return true;
  else if (this.location.path().includes('/settings') && this.authService.checktokeStatus())
      return true;
  this.router.navigate(['/login']);
  return false;
}


}