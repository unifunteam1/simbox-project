import { Injectable } from '@angular/core';

import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MNO } from '../interfaces/MNO';

@Injectable({
  providedIn: 'root'
})
export class MnoListService {

  constructor(private http:HttpClient) { }

  getMNOsListbyId(id) {
    return this.http.get(`${environment.apiUrl}/mnolist?id=`+id)
                .toPromise()
                .then(res => <MNO[]> res)
                .then(data => { return data });
}
}
