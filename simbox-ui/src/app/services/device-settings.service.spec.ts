import { TestBed } from '@angular/core/testing';

import { DeviceSettingsService } from './device-settings.service';

describe('DeviceSettingsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DeviceSettingsService = TestBed.get(DeviceSettingsService);
    expect(service).toBeTruthy();
  });
});
