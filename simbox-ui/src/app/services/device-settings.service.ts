import { Injectable } from '@angular/core';

import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DeviceSettings } from '../interfaces/DeviceSettings';

@Injectable({
  providedIn: 'root'
})
export class DeviceSettingsService {

  pickedDeviceId: number = null;
  pickedMNOId
  constructor(private http: HttpClient) { }

  getdevicesettings() {
    return this.http.get(`${environment.apiUrl}/devicesettings?id=` + this.pickedDeviceId)
      .toPromise()
      .then(res => <DeviceSettings>res)
      .then(data => { return data });
  }
  setdeviceId(id: number) {
    this.pickedDeviceId = id;
  }
}
