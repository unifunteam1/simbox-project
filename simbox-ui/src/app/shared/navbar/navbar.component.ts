import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { ToggleService } from 'src/app/services/toggle.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  isSidebarPinned = false;
  constructor(private authService:AuthService, private toggleService:ToggleService) { }

  ngOnInit() {
  }
  toggleSidebarPin() {
    this.toggleService.toggleSidebarPin();
  }
}
