export interface DeviceSettings{
    id,
    device_id,
    SIP_URI,
    WS_Server,
    password,
    auth_user
}