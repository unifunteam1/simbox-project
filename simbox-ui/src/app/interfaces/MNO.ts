export interface MNO{
    id,
    name,
    device_id,
    dongle_name,
    create_date,
    update_date,
    deleted,
    busy
}