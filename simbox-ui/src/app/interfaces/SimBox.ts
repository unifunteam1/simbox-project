export interface Simbox{
    id,
    name,
    create_date,
    update_date,
    deleted,
    busy
}
