import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SimboxListComponent } from './simbox-list.component';

describe('SimboxListComponent', () => {
  let component: SimboxListComponent;
  let fixture: ComponentFixture<SimboxListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimboxListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimboxListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
