import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthGuardService } from 'src/app/services/auth-guard.service';
import { SimboxListService } from 'src/app/services/simbox-list.service';
import { Simbox } from 'src/app/interfaces/SimBox';
import { DeviceSettingsService } from 'src/app/services/device-settings.service';
import { ToggleService } from 'src/app/services/toggle.service';

@Component({
  selector: 'app-simbox-list',
  templateUrl: './simbox-list.component.html',
  styleUrls: ['./simbox-list.component.css']
})


export class SimboxListComponent implements OnInit {

  simBoxes: Simbox[];

  constructor(private sList: SimboxListService, private router: Router, private authGuard: AuthGuardService, private settings: DeviceSettingsService, private toggleService: ToggleService) { }

  ngOnInit() {
    this.sList.getSimboxes().subscribe((data: Simbox[]) => this.simBoxes = data);
  }

  onClick(simbox) {
    if (!simbox.busy) {
      localStorage.setItem('selecteditem', simbox.id);
      this.router.navigate(['webphone']);
      this.settings.setdeviceId(simbox.id);
    }
    else {
      alert("simbox is busy");
    }
  }
  getClass() {
    const classtoggle = {
      'pinned-sidebar': this.toggleService.getSidebarStat().isSidebarPinned
    }
    return classtoggle;
  }
}

