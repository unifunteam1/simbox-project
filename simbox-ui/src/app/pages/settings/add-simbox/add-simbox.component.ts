import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SimboxListService } from 'src/app/services/simbox-list.service';
import { Simbox } from 'src/app/interfaces/SimBox';

@Component({
  selector: 'app-add-simbox',
  templateUrl: './add-simbox.component.html',
  styleUrls: ['./add-simbox.component.css']
})
export class AddSimboxComponent implements OnInit {

  addForm: FormGroup;
  simbox: Simbox;
  submitted = false;

  constructor(private formBuilder: FormBuilder, private router: Router, private sList: SimboxListService) { }

  ngOnInit() {
    this.addForm = this.formBuilder.group({
      name: ['', Validators.required],
      deleted: ['', Validators.required],
      busy: ['', Validators.required],
    });
  }

  onSubmit() {
    this.submitted = true;
    if (this.addForm.valid)
      this.sList.addSimbox(this.addForm.value).subscribe((data: Simbox) => {
        this.router.navigate(['/settings']);
      });
  }
}
