import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSimboxComponent } from './add-simbox.component';

describe('AddSimboxComponent', () => {
  let component: AddSimboxComponent;
  let fixture: ComponentFixture<AddSimboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddSimboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSimboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
