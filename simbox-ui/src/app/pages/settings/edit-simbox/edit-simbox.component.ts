import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SimboxListService } from 'src/app/services/simbox-list.service';
import { Simbox } from 'src/app/interfaces/SimBox';

@Component({
  selector: 'app-edit-simbox',
  templateUrl: './edit-simbox.component.html',
  styleUrls: ['./edit-simbox.component.css']
})
export class EditSimboxComponent implements OnInit {

  editForm: FormGroup;
  simbox: Simbox;
  ready = false;
  submitted = false;

  constructor(private formBuilder: FormBuilder, private router: Router, private sList: SimboxListService) { }

  ngOnInit() {
    if (localStorage.getItem('editsimbox')) {
      this.sList.getSimboxbyId(localStorage.getItem('editsimbox')).subscribe((data: Simbox) => {this.simbox = data; this.ready = true;});
      this.editForm = this.formBuilder.group({
        name: ['', Validators.required],
        deleted: ['', Validators.required],
        busy: ['', Validators.required],
      });
    }
    else
      this.router.navigate(['settings']);
  }
  onSubmit() {
    this.submitted = true;
    if (this.editForm.valid) {
      this.simbox.name=this.editForm.controls.name.value;
      this.simbox.deleted=this.editForm.controls.deleted.value;
      this.simbox.busy=this.editForm.controls.busy.value;
      this.sList.editSimbox(this.simbox).subscribe((data: Simbox) => {
        this.router.navigate(['/settings']);
      });
    }
  }
}
