import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSimboxComponent } from './edit-simbox.component';

describe('EditSimboxComponent', () => {
  let component: EditSimboxComponent;
  let fixture: ComponentFixture<EditSimboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditSimboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSimboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
