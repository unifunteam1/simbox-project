import { Component, OnInit } from '@angular/core';
import { SimboxListService } from 'src/app/services/simbox-list.service';
import { Router } from '@angular/router';
import { Simbox } from 'src/app/interfaces/SimBox';
import { ToggleService } from 'src/app/services/toggle.service';


@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  simboxes: Simbox[];
  simbox: Simbox;

  constructor(private router: Router, private sList: SimboxListService, private toggleService: ToggleService) { }

  ngOnInit() {
    this.sList.getSimboxes().subscribe((data: Simbox[]) => this.simboxes = data);
  }
  deleteSimbox(simbox) {
    this.sList.deleteSimbox(simbox).subscribe((data: Simbox) => this.simbox = data);
    this.sList.getSimboxes().subscribe((data: Simbox[]) => {this.simboxes = data});
  }
  editSimbox(simbox) {
    localStorage.setItem('editsimbox', simbox.id);
    this.router.navigate(['settings/editsimbox']);
  }
  addSimbox() {
    this.router.navigate(['settings/addsimbox']);
  }

  getClass() {
    const classtoggle = {
      'pinned-sidebar': this.toggleService.getSidebarStat().isSidebarPinned
    }
    return classtoggle;
  }
}
