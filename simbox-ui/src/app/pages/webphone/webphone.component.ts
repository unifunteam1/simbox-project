import { Component, OnInit } from '@angular/core';
import { Simbox } from 'src/app/interfaces/SimBox';
import { MNO } from 'src/app/interfaces/MNO';
import { DeviceSettingsService } from 'src/app/services/device-settings.service';
import { SimboxListService } from 'src/app/services/simbox-list.service';
import { MnoListService } from 'src/app/services/mno-list.service';
import * as SIP from 'sip.js/dist/sip';
import { ToggleService } from 'src/app/services/toggle.service';
import { count } from 'rxjs/operators';


@Component({
  selector: 'app-webphone',
  templateUrl: './webphone.component.html',
  styleUrls: ['./webphone.component.css'],
})
export class WebphoneComponent implements OnInit {
  ua: SIP.UA;
  MNO: MNO[];
  MNOList: MNO[];
  selectedsimBox: Simbox;
  but_value: string = '';
  incomingNumber: string;
  constructor(private settings: DeviceSettingsService, private sList: SimboxListService, private mnoList: MnoListService, private toggleService: ToggleService) { }

  ngOnInit() {
    if (this.settings.pickedDeviceId) {
      this.mnoList.getMNOsListbyId(localStorage.getItem('selecteditem')).then(data => this.MNOList = data);
      this.sList.getSimboxbyId(this.settings.pickedDeviceId).subscribe((data) => this.selectedsimBox = data);
    }
    }
  uaconstruction() {
    console.log("Succes connecting to ws");
    if (this.settings.pickedMNOId != null) {
      this.settings.getdevicesettings().then(data => {
        this.ua = new SIP.UA({
          uri: data[0].SIP_URI,
          transportOptions: {
            wsServers: [data[0].WS_Server]
          },
          sessionDescriptionHandlerOptions: {
            iceCheckingTimeout: 500
          },
          authorizationUser: data[0].auth_user,
          password: atob(data[0].password)
        });
        this.call();
        this.submit_btn("5");
        this.incomingNumber = " " + data[0].SIP_URI.substring(0,4);
      });
    }
    else {
      alert("select mno please");
    }
  }
  call() {
    let session = this.ua.invite((<HTMLInputElement>document.getElementById('ua-uri')).value, {
      sessionDescriptionHandlerOptions: {
        constraints: {
          audio: true,
          video: false
        }
      },

    });
    let remoteAudio = <HTMLAudioElement>document.getElementById('remoteAudio');
    let localAudio = <HTMLAudioElement>document.getElementById('localAudio');
    this.ua.on('invite', (session) => session.accept());
    session.on('trackAdded', function () {

    
    // We need to check the peer connection to determine which track was added
      let pc = session.sessionDescriptionHandler.peerConnection;

      // Gets remote tracks
      let remoteStream = new MediaStream();
      pc.getReceivers().forEach(function (receiver) {
        remoteStream.addTrack(receiver.track);
      });
      remoteAudio.srcObject = remoteStream;
      remoteAudio.play();

     

      // Gets local tracks
      let localStream = new MediaStream();
      pc.getSenders().forEach(function (sender) {
        localStream.addTrack(sender.track);
      });
      localAudio.srcObject = localStream;
      localAudio.play();
    });
  }
  phoneclick(btnvalue: string) {
    let path: string;
    if (btnvalue == 'clean' && (<HTMLInputElement>document.getElementById("ua-uri")).value != '')
      (<HTMLInputElement>document.getElementById("ua-uri")).value = '';
    else if (btnvalue == 'back' && (<HTMLInputElement>document.getElementById("ua-uri")).value != '')
      (<HTMLInputElement>document.getElementById("ua-uri")).value = (<HTMLInputElement>document.getElementById('ua-uri')).value.slice(0, -1);
    else if ((<HTMLInputElement>document.getElementById("ua-uri")).value.length < 11) {
      (<HTMLInputElement>document.getElementById("ua-uri")).value += btnvalue;
      if (btnvalue == '.')
        path = 'assets/sounds/dot.ogg';
      else if (btnvalue == '@')
        path = 'assets/sounds/at.ogg';
      else
        path = 'assets/sounds/' + btnvalue + '.ogg';
      var audio = new Audio(path);
      audio.play();
    }
  }
  selectmno() {
    this.settings.pickedMNOId = (<HTMLInputElement>document.getElementById("mnolist")).value;

    if (this.settings.pickedMNOId != null) {
      this.settings.getdevicesettings().then(data => {
        this.ua = new SIP.UA({
          uri: data[0].SIP_URI,
          transportOptions: {
            wsServers: [data[0].WS_Server]
          },
          sessionDescriptionHandlerOptions: {
            iceCheckingTimeout: 500
          },
          authorizationUser: data[0].auth_user,
          password: atob(data[0].password)
        });
        // this.call();
      });
    }
    else {
      alert("select mno please");
    }

  }

  getClass() {
    const classtoggle = {
      'pinned-sidebar': this.toggleService.getSidebarStat().isSidebarPinned
    }
    return classtoggle;
  }
 
  submit_btn(sw: string) {
   this.but_value= sw;
  }

  clearMessageArea(){
    (<HTMLInputElement>document.getElementById('smstext')).value = ' ';
  }
}
