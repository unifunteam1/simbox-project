import { Component, OnInit } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { first, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/services/alert.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  loginForm: any;
  errorMessage = "";

  constructor(private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private alert: AlertService) { }

  ngOnInit() {
    if (localStorage.getItem('JWT_TOKEN') != null)
      this.router.navigate(['simboxlist']);
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }
  get f() {
    return this.loginForm.controls;
  }
  onSubmit() {
    if (this.loginForm.valid)
      this.authService.login({
        username: this.f.username.value,
        password: this.f.password.value})
      .pipe(
        first()).subscribe(
          data => {
            this.router.navigate(['/simboxlist']);
          },
          error => {
            this.errorMessage = error;
          }
        );
    else 
      this.alert.setErrorMessage("Complete the form");
  }

}

