import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SimboxListComponent } from './pages/simbox-list/simbox-list.component';
import { WebphoneComponent } from './pages/webphone/webphone.component';
import { LoginComponent } from './pages/login/login.component';
import { AuthGuardService } from './services/auth-guard.service';
import { SettingsComponent } from './pages/settings/settings.component';
import { AddSimboxComponent } from './pages/settings/add-simbox/add-simbox.component';
import { EditSimboxComponent } from './pages/settings/edit-simbox/edit-simbox.component';
const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'simboxlist', component: SimboxListComponent, canActivate: [AuthGuardService] },
  { path: 'webphone', component: WebphoneComponent, canActivate: [AuthGuardService] },
  { path: 'settings', component: SettingsComponent, canActivate: [AuthGuardService] },
  { path: 'settings/addsimbox', component: AddSimboxComponent, canActivate: [AuthGuardService]},
  { path: 'settings/editsimbox', component: EditSimboxComponent, canActivate: [AuthGuardService]},
  {
    path: '**',
    redirectTo: 'login',
    pathMatch: 'full',
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
