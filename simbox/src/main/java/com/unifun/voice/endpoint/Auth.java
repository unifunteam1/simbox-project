package com.unifun.voice.endpoint;

import javax.ws.rs.POST;
import javax.ws.rs.Path;

import com.unifun.voice.helpers.Constants;
import com.unifun.voice.helpers.HelperClass;
import com.unifun.voice.helpers.LDAPLogin;
import com.unifun.voice.jsonmapper.User;
import com.unifun.voice.jwt.TokenManager;
import org.jboss.logging.Logger;

@Path("/auth")
public class Auth {

	private static final Logger logger = Logger.getLogger(Auth.class);
	private static final Long tokenExpiration = (long) 1000 * 60 * 45;

	@POST
	public String getPost(String reqBody) {
		HelperClass helperClass = new HelperClass();
		LDAPLogin ldapLogin = new LDAPLogin();
		TokenManager tokenManager = new TokenManager();

		logger.info(reqBody);
		System.out.println("before ldap");
		User user = ldapLogin.login(helperClass.decodeBase64(reqBody));
		System.out.println("after ldap");
		if(user == null) {
			return Constants.NOT_OK_RESPONSE;
		}
		String token = tokenManager.generateToken(Constants.APP_ID, Constants.TOKEN_ISSUER,
				Constants.TOKEN_SUBJECT, tokenExpiration, user);
		String refresh = tokenManager.generateToken(Constants.APP_ID, Constants.TOKEN_ISSUER,
				Constants.TOKEN_SUBJECT, tokenExpiration * 2, user);
		return tokenManager.tokenResponse(token, refresh);
	}
}