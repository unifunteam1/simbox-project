package com.unifun.voice.endpoint;

import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.unifun.voice.orm.model.MNOs;

@Path("/mnolist")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class MNOlistEndpoint {

	@GET
	public List<MNOs> doGET(@QueryParam("id") int device_id) {
		return MNOs.list("device_id", device_id);
	}
}
