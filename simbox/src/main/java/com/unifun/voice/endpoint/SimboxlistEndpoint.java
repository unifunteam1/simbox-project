package com.unifun.voice.endpoint;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.unifun.voice.helpers.Constants;
import com.unifun.voice.orm.model.SimboxDevices;

@Path("/simboxlist")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class SimboxlistEndpoint {
	@Inject
	EntityManager em;

	@Path("/delete")
	@Transactional
	@POST
	public SimboxDevices delete(SimboxDevices s) {
		s.setDeleted(true);
		s.setUpdate_date(new SimpleDateFormat(Constants.DATE_FORMAT).format(new Date()));
		em.merge(s);
		return s;
	}

	@Path("/getAll")
	@GET
	public List<SimboxDevices> getAll() {
		return SimboxDevices.listAll();
	}

	@Path("/edit")
	@Transactional
	@POST
	public SimboxDevices edit(SimboxDevices s) {
		s.setUpdate_date(new SimpleDateFormat(Constants.DATE_FORMAT).format(new Date()));
		em.merge(s);
		return s;
	}

	@Path("/add")
	@Transactional
	@POST
	public SimboxDevices add(SimboxDevices s) {
		s.setCreate_date(new SimpleDateFormat(Constants.DATE_FORMAT).format(new Date()));
		s.persist();
		return s;
	}
	
	@Path("/getById")
	@GET
	public SimboxDevices doGET(@QueryParam("id") int id) {
		return SimboxDevices.findById(id);
	}
}
