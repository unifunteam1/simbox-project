package com.unifun.voice.endpoint;

import com.unifun.voice.helpers.Constants;
import com.unifun.voice.jwt.TokenManager;

import java.io.IOException;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.HttpMethod;


@ApplicationScoped
@WebFilter(urlPatterns = "/*", filterName = "RequestsFilter")
public class ReqFilter implements Filter {
	@Inject
	TokenManager tokenManager;
	private final String auth_header = "Authorization";
	private final String login_path = "/auth";

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		 final HttpServletResponse servletResponse = (HttpServletResponse) response;
	        final HttpServletRequest servletRequest = (HttpServletRequest) request;

	        servletResponse.setHeader("Access-Control-Allow-Origin", "*");
	        servletResponse.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
	        servletResponse.setHeader("Access-Control-Max-Age", "1000");
	        servletResponse.setHeader("Access-Control-Allow-Headers", "x-requested-with, Content-Type, origin, authorization, accept, client-security-token, Session-Token, Token");
			if (servletRequest.getMethod().equals(HttpMethod.OPTIONS)) {
	            servletResponse.setStatus(HttpServletResponse.SC_ACCEPTED);
	            return;
	        }

		if (!servletRequest.getServletPath().equals(login_path)) {
			// init session if token is valid
			String ipAddr = servletRequest.getRemoteAddr();
			String authBearer = servletRequest.getHeader(auth_header).substring(7);

			String tokenResponse = tokenManager.verifyToken(authBearer, Constants.SECRET_KEY);
			switch (tokenResponse) {

				case Constants.OK_RESPONSE:
					// check if session is not in db and add it.
					chain.doFilter(request, response);
					return;
				case Constants.EXPIRED_RESPONSE:
					// remove session from table.
					System.out.println("Token expired, remove session");
					chain.doFilter(request, response);
					return;
				case Constants.NOT_OK_RESPONSE:
					// unauthorized, token is not valid.
					System.out.println("Invalid token signature");
					chain.doFilter(request, response);
					return;
			}
		} else {
			System.out.println("login page");
			chain.doFilter(request, response);
			return;
		}
	}
}
