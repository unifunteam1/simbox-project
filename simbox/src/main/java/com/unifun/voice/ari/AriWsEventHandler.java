package com.unifun.voice.ari;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import ch.loway.oss.ari4java.generated.*;

import ch.loway.oss.ari4java.tools.ARIException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.loway.oss.ari4java.tools.AriCallback;
import ch.loway.oss.ari4java.tools.RestException;



public class AriWsEventHandler implements AriCallback<Message> {

    protected Map<String,Bridge> bridges = new ConcurrentHashMap<>();





    private Bridge b = null;
    private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());
    List<AriWsEventHandler> listeners = new ArrayList<AriWsEventHandler>();
    private AriManager aim;
    ExecutorService exec = Executors.newFixedThreadPool(10);


    public AriWsEventHandler(AriManager aim) {
        this.aim = aim;

    }


    @Override
    public void onSuccess(Message result) {

        exec.execute(()->{ariEventHandler(result);});
    }

    private void ariEventHandler(Message result) {
    try {
        logger.debug("received Message {}", result.getClass());
        if (result instanceof StasisStart) {
            onStasisStart((StasisStart) result);
        } else if (result instanceof StasisEnd) {
            onStasisEnd((StasisEnd) result);
        } else if (result instanceof ChannelStateChange) {
            onChannelStateChange((ChannelStateChange) result);
//        }
//        else if (result instanceof PlaybackStarted) {
//            onPlaybackStarted((PlaybackStarted) result);
//        } else if (result instanceof PlaybackFinished) {
//            onPlaybackFinished((PlaybackFinished) result);
        } else if (result instanceof ChannelDtmfReceived) {
            onDtmfReceived((ChannelDtmfReceived) result);
        } else if (result instanceof ChannelVarset) {
            // FIXME
            logger.warn("Event {} not handlet yet", result.getClass().getSimpleName());
        } else if (result instanceof ChannelHangupRequest) {
            onChannelHangupRequest((ChannelHangupRequest) result);


        } else if (result instanceof ChannelCreated) {
            onChannelCreated((ChannelCreated) result);}
        else {
            logger.warn("Event {} not handlet yet", result.getClass().getSimpleName());

        }
    }catch (Exception e)

    { return;}

    }




    private void onChannelCreated(ChannelCreated event) {
//        try {
//
//            Channel channel = event.getChannel();
//            String channelId = channel.getId();
//            String applicationName = event.getApplication();
//            logger.info(
//                    "onChannelCreated: received message with channelId {} on application {}, channel state {}"
//                            + "calling number {}, called number {}",
//                    channelId, applicationName, channel.getState(), channel.getCaller().getNumber(),
//                    channel.getDialplan().getExten());
//
//        } catch (Exception e) {
//            logger.error("Unknown Error", e);
//        }

    }

    @Override
    public void onFailure(RestException e) {
        logger.error("Some ");

    }


    Set<String> achannel = new HashSet<>();

    private void onStasisStart(StasisStart event) {

        try {

            Channel channel = event.getChannel();
            String channelId = channel.getId();
            String applicationName = event.getApplication();
            logger.info(
                    "onStasisStart: received message with channelId {} on application {}, channel state {}"
                            + "called number {}, calling number {}",
                    channelId, applicationName, channel.getState(),
                    channel.getDialplan().getExten(),
                    channel.getCaller().getNumber());
            if(achannel.contains(channelId)){
                logger.info("Channel {} is added in bridge", channelId);
                return;
            }
            // create a bridge and start playing MOH on it
            // UGLY: we should have a constant for the allowed bridge types




                logger.info("Init Bridge");
                    // Bridge b = aim.getAri().bridges().get("bridge-1561539042.0");
                b = aim.getAri().bridges().create("", "bridge-" + channelId, "b" + channelId);
               logger.info("Bridge ID:" + b.getId() + " Name:" + b.getName() + " Tech:" + b.getTechnology()
                       + " Creator:" + b.getCreator());

            logger.info( "Listing bridges");
            List<Bridge> bridges = aim.getAri().bridges().list();



            // start MOH on the bridge
            logger.info("Starting MOH on bridge");
            aim.getAri().bridges().startMoh(b.getId(), "");
            //Create new channel with b subscriner
            String dummy = "";
            Channel chanB = aim.getAri().channels().originate("SIP/"+channel.getDialplan().getExten(), channel.getDialplan().getExten(), "unifun", 1, dummy, aim.getAri().getAppName(), dummy, channel.getCaller().getNumber(), 1000, Collections.EMPTY_MAP, dummy, dummy, dummy,dummy);
//            Channel chanB = aim.getAri().channels().originate("SIP/"+channel.getDialplan().getExten(), channel.getDialplan().getExten(), "unifun", 1,dummy, dummy, dummy, dummy, 10000, Collections.EMPTY_MAP, dummy, dummy, dummy);
//            logger.info("ZZZZZZZZZZZZZZZZZZZZZZZ1" + channel.getDialplan().getExten() + "NUMBER " + channel.getCaller().getNumber() + "ZAZA" +channel.getDialplan().getContext());
//            "SIP/"+channel.getDialplan().getExten()
            achannel.add(chanB.getId());

            logger.info("Channel:" + chanB.getId() + " in state " + chanB.getState());
            aim.getAri().bridges().addChannel(b.getId(), channelId, "");

            aim.getAri().bridges().addChannel(b.getId(), chanB.getId(), "");

            ActionChannels ac = aim.getAri().getActionImpl(ActionChannels.class);
            ac.answer(channelId);
            ac.dial(channel.getId(), chanB.getId(), 1000);

            ac.ring(chanB.getId());


            aim.getAri().closeAction(ac);

        }
        catch (Exception e) {
            logger.error("Unknown Error", e);
        }

    }


    public void onDtmfReceived(ChannelDtmfReceived event) {
        Channel channel = event.getChannel();
        logger.info("Channel {}, Received digit {}", channel.getId(), event.getDigit());
    }


    public void onChannelStateChange(ChannelStateChange event) {
        Channel channel = event.getChannel();
        logger.info("Channel {} is now {}", channel.getName(), channel.getState());
    }


//    private void onPlaybackStarted(PlaybackStarted event) {
//        logger.info("Start palayback");
//
//    }
//
//
//    private void onPlaybackFinished(PlaybackFinished event) {
//        logger.info("Stop palayback");
//
//
//    }


    private void onStasisEnd(StasisEnd event) {
//        Channel chanB = event.getChannel();
//        Channel channel = event.getChannel();
//
//        if(achannel.contains(chanB.getId())){
//            achannel.remove(chanB.getId());
//        }
//        if(achannel.contains(channel.getId())){
//            achannel.remove(channel.getId());
//        }
    }


    private void onChannelHangupRequest(ChannelHangupRequest event) {
        Channel channel = event.getChannel();
        Channel chanB = event.getChannel();
        try {
            if (b.getId()!= null){
            ActionChannels ac = aim.getAri().getActionImpl(ActionChannels.class);
            ac.hangup(chanB.getId(), channel.getId()); }

        } catch (ARIException e) {
            e.printStackTrace();
        }


        if (achannel.contains(channel.getId())){
        logger.info("There are channels in bridge");}
        else if (b.getId()!= null)
        {
            logger.info("Removing bridge....");

            try {
                aim.getAri().bridges().destroy(b.getId());
            } catch (RestException e) {
                e.printStackTrace();
            }
        }




    }




}


// Playback pb = ac.play(channelId, "sound:hello-world", "", 0, 0, "");
// achannel.put(pb.getId(), new Temp(channel, null));
// Channel chanB = aim.getAri().channels().r//create(channel.getDialplan().getExten(), aim.getAri().getAppName(), dummy, dummy, dummy, channel.getCaller().getNumber(), dummy);
//            Channel chanB = aim.getAri().channels().originate(channel.getDialplan().getExten(), "unifun", "unifun", 1, dummy, dummy, dummy, channel.getCaller().getNumber(), 1000, Collections.EMPTY_MAP, dummy, dummy, dummy);
