package com.unifun.voice.jsonmapper;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BridgeResponseNew {
    private String id ;
    private String technology;
    private String bridge_type;
    private String bridge_class;
    private String creator;
    private String name;
    private String[] channels;
    private String video_mode;
}
