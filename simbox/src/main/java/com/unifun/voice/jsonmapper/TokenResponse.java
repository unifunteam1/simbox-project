package com.unifun.voice.jsonmapper;

import lombok.Data;

@Data
public class TokenResponse {
    private String jwt;
    private String refreshToken;
}
