package com.unifun.voice.jsonmapper;

import lombok.Data;

@Data
public class User {
    private String username;
    private String password;
}