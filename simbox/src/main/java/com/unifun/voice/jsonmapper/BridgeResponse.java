package com.unifun.voice.jsonmapper;

import lombok.Data;

import java.util.Date;
@Data
public class BridgeResponse {
    private String id ;
    private String technology;
    private String bridge_type;
    private String bridge_class;
    private String creator;
    private String name;
    private String[] channels;
    private Date creationtime;
    private String video_mode;
}


/* {
  "id": "bridge-1561535348.22",
  "technology": "simple_bridge",
  "bridge_type": "mixing",
  "bridge_class": "stasis",
  "creator": "Stasis",
  "name": "b1561535348.22",
  "channels": [],
  "creationtime": "2019-06-26T10:49:08.702+0300",
  "video_mode": "talker"
}*/


