package com.unifun.voice.jwt;

import com.unifun.voice.helpers.Constants;
import com.unifun.voice.jsonmapper.TokenResponse;
import com.unifun.voice.jsonmapper.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.NoArgsConstructor;

import javax.crypto.spec.SecretKeySpec;
import javax.enterprise.context.ApplicationScoped;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import java.util.Date;

@ApplicationScoped
@NoArgsConstructor
public class TokenManager {

    public String verifyToken(String jwt, String secretKey) {
        try {
            Claims claims = Jwts.parser()
                    .setSigningKey(DatatypeConverter.parseBase64Binary(secretKey))
                    .parseClaimsJws(jwt).getBody();
            return Constants.OK_RESPONSE;

        } catch (io.jsonwebtoken.security.SignatureException e) {
            return Constants.NOT_OK_RESPONSE;

        } catch (io.jsonwebtoken.ExpiredJwtException e) {
            return Constants.EXPIRED_RESPONSE;
        }
    }

    public String generateToken(String id, String issuer, String subject, long ttlMillis, User user) {

        //The JWT signature algorithm we will be using to sign the token
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);

        //We will sign our JWT with our ApiKey secret
        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(Constants.SECRET_KEY);
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

        //Let's set the JWT Claims
        JwtBuilder builder = Jwts.builder().setId(id)
                .setIssuedAt(now)
                .setSubject(subject)
                .setIssuer(issuer)
                .signWith(signatureAlgorithm, signingKey);

        //if it has been specified, let's add the expiration, ex: 60 * 1000 * 1 = 1 min
        if (ttlMillis > 0) {
            long expMillis = nowMillis + ttlMillis;
            Date exp = new Date(expMillis);
            builder.setExpiration(exp);
        }

        //Builds the JWT and serializes it to a compact, URL-safe string
        String token = builder.compact();
        return token;
    }

    public String tokenResponse(String token, String refreshToken) {
        TokenResponse tokenResponse = new TokenResponse();
        tokenResponse.setJwt(token);
        tokenResponse.setRefreshToken(refreshToken);
        Jsonb jsonb = JsonbBuilder.create();
        return jsonb.toJson(tokenResponse);
    }
}
