package com.unifun.voice.orm.model;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import lombok.Data;

@Data
@Entity
@Table(name = "login_log")
public class LoginLog extends PanacheEntityBase {
	@Id
	@SequenceGenerator(name = "LoginLogSequence", sequenceName = "login_log_id_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "LoginLogSequence")
	int id;
	String username;
	String date;

	public LoginLog(String username) {
		this.username = username;
		this.date = new Timestamp(new Date().getTime()).toString();
	}
}
