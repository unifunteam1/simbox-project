package com.unifun.voice.orm.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import lombok.Data;

@Data
@Entity
@Table(name = "simbox_devices")
public class SimboxDevices extends PanacheEntityBase {

	@Id
	@SequenceGenerator(name = "simboxDevicesSequence", sequenceName = "simbox_devices_id_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "simboxDevicesSequence")
	int id;
	String name;
	String create_date;
	String update_date;
	boolean deleted;
	boolean busy;
}
