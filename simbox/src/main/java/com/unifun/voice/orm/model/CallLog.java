package com.unifun.voice.orm.model;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@NoArgsConstructor
@Data
@Entity
@Table(name = "call_log")
public class CallLog extends PanacheEntityBase {
    @Id
    @SequenceGenerator(name = "CallLogSequence", sequenceName = "call_log_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CallLogSequence")
    int id;

    String callerId;
    String dialedNumber;
    Date timestamp;
    long callDuration;
}
