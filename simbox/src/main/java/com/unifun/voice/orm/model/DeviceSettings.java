package com.unifun.voice.orm.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import lombok.Data;

@Data
@Entity
@Table(name = "device_settings")
public class DeviceSettings extends PanacheEntityBase {

	@Id
	@SequenceGenerator(name = "deviceSettingsSequence", sequenceName = "device_settings_id_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "deviceSettingsSequence")
	int id;
	int device_id;
	String SIP_URI;
	String WS_Server;
	String password;
	String auth_user;
}
