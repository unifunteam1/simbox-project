package com.unifun.voice.helpers;

public class Constants {

    public static final String APP_ID = "SIMBOX_APP_001";
    public static final String TOKEN_ISSUER = "SIMBOX_BE_001";
    public static final String TOKEN_SUBJECT = "Auth-Token";

    public static final String SECRET_KEY = "dc98a44e-729f-4c13-bc83-e807b308c1b995a7c629-e229-4bb7-9221"+
            "-a8c530e596841e530f0c-715b-40d1-b402-e44e68d61d09"; //temporary
    public static final String OK_RESPONSE = "ok";
    public static final String NOT_OK_RESPONSE = "notok";
    public static final String EXPIRED_RESPONSE = "expired";
    public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
}
