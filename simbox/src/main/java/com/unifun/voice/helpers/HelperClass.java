package com.unifun.voice.helpers;

import com.unifun.voice.jsonmapper.BridgeResponse;
import com.unifun.voice.jsonmapper.BridgeResponseNew;
import lombok.NoArgsConstructor;

import javax.enterprise.context.ApplicationScoped;
import java.util.Base64;
import java.util.UUID;

@NoArgsConstructor
public class HelperClass {

    public String genUUID() {
        String resultUUID = UUID.randomUUID().toString();
        resultUUID += UUID.randomUUID().toString();
        resultUUID += UUID.randomUUID().toString();
        System.out.println(resultUUID);
        return resultUUID;
    }

    public String decodeBase64(String request) {
        try {
            byte[] requestBytes = request.getBytes("UTF-8");
            byte[] decodedRequest = Base64.getDecoder().decode(requestBytes);
            String result = new String(decodedRequest);
            return result;
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }

    public BridgeResponseNew initbridgeresponse (BridgeResponse bridgeresponse) {
        BridgeResponseNew bridgeResponseNew = new BridgeResponseNew ();
        bridgeResponseNew.setId(bridgeresponse.getId());
        bridgeResponseNew.setTechnology(bridgeresponse.getTechnology());
        bridgeResponseNew.setBridge_type(bridgeresponse.getBridge_type());
        bridgeResponseNew.setBridge_class(bridgeresponse.getBridge_class());
        bridgeResponseNew.setCreator(bridgeresponse.getCreator());
        bridgeResponseNew.setName(bridgeresponse.getName());
        bridgeResponseNew.setChannels(bridgeresponse.getChannels());
        bridgeResponseNew.setVideo_mode(bridgeresponse.getVideo_mode());

        return bridgeResponseNew;
    }
}


