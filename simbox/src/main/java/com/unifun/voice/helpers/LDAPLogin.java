package com.unifun.voice.helpers;

import com.unifun.voice.endpoint.Auth;
import com.unifun.voice.jsonmapper.User;
import com.unifun.voice.orm.model.LoginLog;
import com.unifun.voice.yaml.LdapConfig;
import lombok.NoArgsConstructor;
import org.yaml.snakeyaml.Yaml;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.naming.Context;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.transaction.Transactional;
import java.io.InputStream;
import java.util.Hashtable;

@NoArgsConstructor
public class LDAPLogin {

    public User login(String reqBody) {
        Jsonb jsonb = JsonbBuilder.create();
        User user = jsonb.fromJson(reqBody, User.class);
        String username = user.getUsername();
        String password = user.getPassword();
        // load ldapconf from ldap.yml
        Yaml yaml = new Yaml();
        LdapConfig ldapconf = null;
        try (InputStream in = Auth.class.getResourceAsStream("/ldap.yml")) {
            ldapconf = yaml.loadAs(in, LdapConfig.class);
        } catch (Exception e) {
        }

        try {
            String PROVIDER_URL = "ldap://" + ldapconf.getHost() + ":" + ldapconf.getPort();
            String SECURITY_PRINCIPAL = "cn=" + username + "," + ldapconf.getUsersdn() + "," + ldapconf.getBasedn();

            Hashtable<String, String> env = new Hashtable<String, String>();
            env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
            env.put(Context.PROVIDER_URL, PROVIDER_URL);
            env.put(Context.SECURITY_AUTHENTICATION, "simple");
            env.put(Context.SECURITY_PRINCIPAL, SECURITY_PRINCIPAL);
            env.put(Context.SECURITY_CREDENTIALS,password);
            DirContext ctx = new InitialDirContext(env);
            ctx.close();
            addLoginLog(username);
            return user;
        }
        catch(Exception e) {
            System.out.println("Ldap Login failed");
            return null;
        }
    }

    @Transactional
    private void addLoginLog(String username) {
        LoginLog log = new LoginLog(username);
        log.persist();
    }
}
