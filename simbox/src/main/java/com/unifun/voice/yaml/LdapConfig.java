package com.unifun.voice.yaml;

import lombok.Data;

@Data
public class LdapConfig {

	private String host, port, basedn, usersdn, group;
}
